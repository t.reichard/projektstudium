from pathlib import Path

from models import promptTemplates
from models import entityTypes
import torch

from components.ProductionComponent.ProductionService import ask_question_to_llm
from components.SetUpComponent.SetUpService import set_up_chain
from components.StorageRetrievalComponent.StorageRetrievalService import get_relevant_chunks, reorder_documents
from components.TextCleanUpComponent.CleanUpService import (delete_empty_pages, clean_text, merge_hyphenated_words,
                                                            fix_newlines, remove_multiple_newlines)
from components.TextExtractioncomponent.ExtractionService import load_pdf
from components.TextSplittingComponent.SplittingService import split_docs
from dotenv import load_dotenv
import os


"""
@Author: Max, Thiemo, Jonas , Jonathan
"""
load_dotenv()

user_name = os.getenv('add_user_name_here')
file_name = os.getenv('FILES_BARILLA19')

# Specify from which PDF file the text is to be extracted
folder_path: str = (Path(__file__).parent / f'resources/PDFs/{file_name}.pdf').as_posix()
# Define directory where the ki models life
cache_dir = f"/scratch/users/{user_name}/models/"

# Define which LLM to use
# Link to used model: https://huggingface.co/upstage/SOLAR-0-70b-16bit
llm_id = os.getenv('KIMODELS_LLM')

# Define which embeddings model to use
# Link to used model: https://huggingface.co/BAAI/bge-large-en#using-langchain
embeddings_model_id = os.getenv('KIMODELS_EMBEDDING')

# Define here which chunks should be retrieved as relevant
embeddings_query = "water consumption"


def main() -> None:
    # Clear GPU cache
    torch.cuda.empty_cache()

    # Setup LLM
    chain, prompt = set_up_chain(llm_id, cache_dir, promptTemplates.promptTemplate_Version_few_shot)

    # Step 1: PDF to Text
    documents = load_pdf(folder_path)

    # Step 2: Clean text
    documents = delete_empty_pages(documents)
    cleaning_functions = [
        merge_hyphenated_words,
        fix_newlines,
        remove_multiple_newlines,
    ]
    clean_data = clean_text(documents, cleaning_functions)

    # Step 3: Chunk text
    docs = split_docs(clean_data)

    # Step 4: Embeddings + retrieve relevant chunks
    matching_docs = get_relevant_chunks(embeddings_model_id, docs, embeddings_query)

    # Step 5: Reorder the documents:
    # Less relevant document will be at the middle of the list and more relevant elements at beginning / end.
    reordered_docs = reorder_documents(matching_docs)

    reordered_docs = "\n".join([x.page_content for x in reordered_docs])
    print(f'\nQuestion to LLM:\n{prompt.format(context=reordered_docs, entityTypes=entityTypes.WATER_CONSUMPTION)}')

    # Step 6: Ask question to LLM
    ask_question_to_llm(reordered_docs, chain, entityTypes.WATER_CONSUMPTION)


if __name__ == '__main__':
    main()

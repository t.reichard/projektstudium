promptTemplate_Version_zero_shot_short = """
You are a Named-entity recognition (NER) system that parses the text for key-value performance indicators on corporate social responsibility reports. You take text chunks as inputs and extract and classify them into entity types that might be similar but should not be limited to the following entities:
{entityTypes}
Your output is structured in JSON format, containing the label, the numerical value and the given year and contains no additional information except for the JSON-String.
Context text chunks: {context}
Your JSON-Output:

"""


promptTemplate_Version_zero_shot_output_format = """
System: You are a highly intelligent and accurate Corporate Sustainability Reports domain Named-entity recognition(NER) system. You take the provided text chunks as input and your task is to recognize and extract specific types of Corporate Sustainability Reports domain named entities in that given chunks of text and classify into a set of following predefined entity types:
{entityTypes}
Your output format is only [{{"T": type of entity from predefined entity types, "V": value in the input text, "Y": Year referring to the value in the input text (only apply if the year is given)}}] form, no other form. Don't include any text in response such as 'here are named entities..' etc, return only valid json. Think step by step.
User: Text chunks: {context}
Assistant:
"""

promptTemplate_Version_zero_shot_output_format_Version_2 = """
System: You are a highly intelligent and accurate Corporate Sustainability Reports domain information extraction system. You take the provided text chunks as input and your task is to recognize and extract specific types of Corporate Sustainability Reports domain named entities in that given chunks of text and classify into a set of following predefined entity types. You may customize the predefined entity types to get the desired information extracted from the provided text chunks:
{entityTypes}
Your output format is only [{{"T": type of entity from predefined entity types, "V": value in the input text, "Y": Year referring to the value in the input text (only apply if the year is given)}}] form, no other form. Don't include any text in response such as 'here are named entities..' etc, return only valid json. Think step by step.
User: Text chunks: {context}
Assistant:
"""

promptTemplate_Version_zero_shot_no_output_format = """
System: You are a highly intelligent and accurate Corporate Sustainability Reports domain Named-entity recognition (NER) system. You take the provided text chunks as input and your task is to recognize and extract specific types of Corporate Sustainability Reports domain named entities in that given chunks of text and classify into a set of following predefined entity types:
{entityTypes}
Don't include any text in response such as 'here are named entities..' etc, return only valid json. Think step by step.
User: Text chunks: {context}
Assistant:
"""

promptTemplate_Version_few_shot = """
System: You are a highly intelligent and accurate Corporate Sustainability Reports domain Named-entity recognition(NER) system. You take the provided text chunks as input and your task is to recognize and extract specific types of Corporate Sustainability Reports domain named entities in that given chunks of text and classify into a set of following predefined entity types:
{entityTypes}
Your output format is only [{{"T": type of entity from predefined entity types, "E": entity in the input text, "Y": Year referring to the entity in the input text}}] form, no other form. Don't include any text in response such as 'here are named entities..' etc, return only valid json. Think step by step.
Examples:

User: Text chunks: to halving our  carbon footprint from 2016 to 2030,  and to be fossil-free by 2045.  59  Goods  transport to  customer 15  Goods transport,  imports 4% Work machinery and   own business vehicles  10%  Business travel 12  Electricity and  heating Ahlsell's climate impact originates primarily from freight  transportation to customers and from imports, business travel  and energy use in our facilities. The circle represents Ahlsell's  total climate impact measured in carbon dioxide equivalents. Ahlsell's climate impact 2017 2018 2019 2020 1.16 1.42  1.07 0.89 One of our climate goals is to halve our  carbon footprint by 2030 compared with  2016 and to be fossil-free by 2045.
Assistant: [{{"T": "Carbon footprint in tonnes of carbon dioxide equivalents per MSEK net sales (scopes 1, 2 and 3)", "E": "1.49", "Y": "2016"}}, {{"T": "Carbon footprint in tonnes of carbon dioxide equivalents per MSEK net sales (scopes 1, 2 and 3)", "E": "1.42", "Y": "2017"}}, {{"T": "Carbon footprint in tonnes of carbon dioxide equivalents per MSEK net sales (scopes 1, 2 and 3)", "E": "1.16", "Y": "2018"}}, {{"T": "Carbon footprint in tonnes of carbon dioxide equivalents per MSEK net sales (scopes 1, 2 and 3)", "E": "1.07", "Y": "2019"}}, {{"T": "Carbon footprint in tonnes of carbon dioxide equivalents per MSEK net sales (scopes 1, 2 and 3)", "E": "0.89", "Y": "2020"}}]

User: Text chunks: {context}
Assistant:
"""

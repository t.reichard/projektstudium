from langchain import LLMChain
from models import entityTypes


def ask_question_to_llm(context: str, chain: LLMChain, entityTypes=''):
    response = chain.run(
        {
            'context': context,
            'entityTypes': entityTypes
        }
    )
    print(f'\nLLM answer:\n{response}')

from typing import List

from langchain.document_loaders import PyMuPDFLoader
from langchain.schema import Document


def load_pdf(path: str) -> List[Document]:
    """Load PDF using PyMuPDF."""
    loader = PyMuPDFLoader(path)
    documents = loader.load()
    print('PDF loaded!')
    return documents

import transformers
from langchain import PromptTemplate, HuggingFacePipeline, LLMChain
from transformers import AutoTokenizer, AutoModelForCausalLM
import torch

from models import promptTemplates


def set_up_chain(llm_id: str, cache_dir: str, promptTemplate: str) -> [LLMChain, PromptTemplate]:
    # Load Tokenizer
    tokenizer = AutoTokenizer.from_pretrained(llm_id)
    # Load Model
    model = AutoModelForCausalLM.from_pretrained(llm_id, cache_dir=cache_dir,
                                                 torch_dtype=torch.bfloat16, load_in_8bit=True, trust_remote_code=True,
                                                 device_map="auto",
                                                 offload_folder="offload",
                                                 rope_scaling={"type": "dynamic",
                                                               "factor": 2})  # allows handling of longer inputs with SOLAR-0-70b-16bit
    # Set PT model to inference mode
    model.eval()
    pipeline = build_pipeline(model, tokenizer)
    # Pass hugging face pipeline to langchain class
    llm = HuggingFacePipeline(pipeline=pipeline)
    # Build stacked LLM chain i.e. prompt-formatting + LLM
    prompt = set_up_prompt(promptTemplate)
    chain = LLMChain(llm=llm, prompt=prompt)
    print('Model set up!')
    return chain, prompt


def build_pipeline(model, tokenizer):
    # Build HF Transformers pipeline
    pipeline = transformers.pipeline(
        "text-generation",
        model=model,
        tokenizer=tokenizer,
        device_map="auto",
        max_length=10000,
        do_sample=True,
        top_k=10,
        num_return_sequences=1,
        eos_token_id=tokenizer.eos_token_id
    )
    print('Pipeline created!')
    return pipeline


def set_up_prompt(promptTemplate: str) -> PromptTemplate:
    prompt = PromptTemplate(
        input_variables=['context', 'entityTypes'],
        template=promptTemplate
    )
    return prompt

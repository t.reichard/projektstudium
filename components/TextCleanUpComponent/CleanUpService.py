import re
from typing import Callable, List, Tuple

from langchain.schema import Document


def merge_hyphenated_words(text: str) -> str:
    """Merge hyphenated words split across lines."""
    return re.sub(r"(\w)-\n(\w)", r"\1\2", text)


def fix_newlines(text: str) -> str:
    """Remove newlines that are not preceded or followed by another newline."""
    return re.sub(r"(?<!\n)\n(?!\n)", " ", text)


def remove_multiple_newlines(text: str) -> str:
    """Remove multiple newlines."""
    return re.sub(r"\n{2,}", "\n", text)


def delete_empty_pages(docs: List[Document]) -> List[Document]:
    return [doc for doc in docs if doc.page_content]


def clean_text(docs: List[Document], cleaning_functions: List[Callable[[str], str]]) -> List[Tuple[str]]:
    """Clean text using a list of cleaning functions."""
    for doc in docs:
        for cleaning_function in cleaning_functions:
            if doc.page_content is not None and doc.page_content != '':
                doc.page_content = cleaning_function(doc.page_content)
        # cleaned_pages.append(text)
    print('Text cleaned!')
    return docs
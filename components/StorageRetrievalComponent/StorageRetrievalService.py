from typing import Sequence

from langchain.document_transformers import LongContextReorder
from langchain.embeddings import SentenceTransformerEmbeddings
from langchain.schema import Document
from langchain.vectorstores import Chroma

EMBEDDINGS_INSTRUCTION = "Represent this sentence for searching relevant passages: "


def get_relevant_chunks(embeddings_model_id, docs, embeddings_query):
    embeddings = SentenceTransformerEmbeddings(model_name=embeddings_model_id)
    db = Chroma.from_documents(docs, embeddings)
    matching_docs = db.similarity_search(EMBEDDINGS_INSTRUCTION + embeddings_query, k=5)
    return matching_docs


def reorder_documents(matching_docs) -> Sequence[Document]:
    reordering = LongContextReorder()
    reordered_docs = reordering.transform_documents(matching_docs)
    # Take the two most important documents
    reordered_docs = reordered_docs[0], reordered_docs[-1]
    return reordered_docs

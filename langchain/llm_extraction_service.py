import os
import re
import sys
from typing import List, Tuple, Callable

import torch
import transformers
from langchain import HuggingFacePipeline
from langchain import PromptTemplate, LLMChain
from langchain.document_loaders import PyMuPDFLoader
from langchain.document_transformers import (
    LongContextReorder,
)
from langchain.embeddings import SentenceTransformerEmbeddings
from langchain.schema import Document
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores import Chroma
from transformers import AutoTokenizer, AutoModelForCausalLM

"""
@Author: Max Gnewuch
You need to install the following packages with pip to run this script:
pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu117 --upgrade
"""

# folder_path = r'c:\Users\maxmartin.gnewuch\Projektstudium\Test_PDF\Ahlsell_2020_sm.pdf'
folder_path = sys.argv[1]

# Define Model ID
model_id = "tiiuae/falcon-7b-instruct"

# Define your question
QUESTION = "How high is the  Carbon footprint in tonnes of carbon dioxide equivalents per MSEK net sales (scopes 1, 2 and 3) in 2020?"

# Define your template
promptTemplate = """
Answer the question based on the context below. If the question cannot be answered using the information provided answer with "I don't know".
Keep in mind the context was a table bevor that lost his structure!
Context: {context}
Question: {query}
Answer:
"""

# Define your embeddings question
embeddings_query = "Carbon footprint in tonnes of carbon dioxide"


def load_pdf(path: str) -> List[Document]:
    """Load PDF using PyMuPDF."""
    loader = PyMuPDFLoader(path)
    documents = loader.load()
    print('PDF loaded!')
    return documents


def merge_hyphenated_words(text: str) -> str:
    """Merge hyphenated words split across lines."""
    return re.sub(r"(\w)-\n(\w)", r"\1\2", text)


def fix_newlines(text: str) -> str:
    """Remove newlines that are not preceded or followed by another newline."""
    return re.sub(r"(?<!\n)\n(?!\n)", " ", text)


def remove_multiple_newlines(text: str) -> str:
    """Remove multiple newlines."""
    return re.sub(r"\n{2,}", "\n", text)


def delete_empty_pages(docs: List[Document]) -> List[Document]:
    return [doc for doc in docs if doc.page_content]


def clean_text(docs: List[Document], cleaning_functions: List[Callable[[str], str]]) -> List[Tuple[str]]:
    """Clean text using a list of cleaning functions."""
    cleaned_pages = []
    text = ''
    for doc in docs:
        for cleaning_function in cleaning_functions:
            if doc.page_content is not None and doc.page_content != '':
                doc.page_content = cleaning_function(doc.page_content)
        # cleaned_pages.append(text)
    print('Text cleaned!')
    return docs


def split_docs(documents, chunk_size=1000, chunk_overlap=20):
    text_splitter = RecursiveCharacterTextSplitter(chunk_size=chunk_size, chunk_overlap=chunk_overlap)
    docs = text_splitter.split_documents(documents)
    print('Documents splited!')
    return docs


def set_up_model():
    # Get the absolute path of the current script
    script_dir = os.path.dirname(os.path.abspath(__file__))
    # Go up one directory from the script's location
    parent_dir = os.path.dirname(script_dir)
    # Construct the generic file path
    cache_dir = os.path.join(parent_dir, "models\\")
    # Load Tokenizer
    tokenizer = AutoTokenizer.from_pretrained(model_id)
    # Load Model 
    model = AutoModelForCausalLM.from_pretrained(model_id, cache_dir=cache_dir,
                                                 torch_dtype=torch.bfloat16, trust_remote_code=True, device_map="auto",
                                                 offload_folder="offload")
    # Set PT model to inference mode
    model.eval()
    print('Model set up!')
    return model, tokenizer


def build_pipeline(model, tokenizer):
    # Build HF Transformers pipeline 
    pipeline = transformers.pipeline(
        "text-generation",
        model=model,
        tokenizer=tokenizer,
        device_map="auto",
        max_length=800,
        do_sample=True,
        top_k=10,
        num_return_sequences=1,
        eos_token_id=tokenizer.eos_token_id
    )
    print('Pipeline created!')
    return pipeline


def reorder_documents(matching_docs) -> List[Document]:
    reordering = LongContextReorder()
    reordered_docs = reordering.transform_documents(matching_docs)
    # take the first and last chunk
    reordered_docs = reordered_docs[0], reordered_docs[-1]
    return reordered_docs


def main() -> None:
    model, tokenizer = set_up_model()
    pipeline = build_pipeline(model, tokenizer)
    # Setup prompt template
    prompt = PromptTemplate(
        input_variables=['context', 'query'],
        template=promptTemplate)
    # Pass hugging face pipeline to langchain class
    llm = HuggingFacePipeline(pipeline=pipeline)
    # Build stacked LLM chain i.e. prompt-formatting + LLM
    chain = LLMChain(llm=llm, prompt=prompt)

    documents = load_pdf(folder_path)
    documents = delete_empty_pages(documents)
    # Step 2: Clean text
    cleaning_functions = [
        merge_hyphenated_words,
        fix_newlines,
        remove_multiple_newlines,
    ]
    clean_data = clean_text(documents, cleaning_functions)
    docs = split_docs(clean_data)
    embeddings = SentenceTransformerEmbeddings(model_name="all-MiniLM-L6-v2")
    db = Chroma.from_documents(docs, embeddings)
    matching_docs = db.similarity_search(embeddings_query)
    # Reorder the documents:
    # Less relevant document will be at the middle of the list and more relevant elements at beginning / end.
    reordered_docs = reorder_documents(matching_docs)
    reordered_docs = "\n".join([x.page_content for x in reordered_docs])
    print(f'\nQuestion to LLM:\n{prompt.format(context=reordered_docs, query=QUESTION)}')
    # Test LLMChain
    response = chain.run(
        {
            'context': reordered_docs,
            'query': QUESTION
        }
    )
    print(f'\nLLM anwer:\n{response}')


if __name__ == '__main__':
    main()

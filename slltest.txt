#!/bin/bash
#SBATCH --job-name=llm_test
#SBATCH --output=job-%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH --ntasks=1
#SBATCH --mem=32G
#SBATCH --time=00:30:00

# Load any necessary modules or activate the desired Python environment

# Execute the Python script with the input file from the array
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
pip install torch torchvision torchaudio transformers einops accelerate bitsandbytes sentence_transformers chromadb
python3 ~/prostud/projektstudium/LLM_API/llm_and_langchain.py
